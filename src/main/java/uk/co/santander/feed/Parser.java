package uk.co.santander.feed;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface Parser {
	Optional<List<String>> parse(String message);

	Optional<Double> parseDouble(String value);

	Optional<LocalDateTime> parseDate(String date, String time);
}
