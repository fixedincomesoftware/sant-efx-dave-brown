package uk.co.santander.feed;

import java.util.Optional;

public interface MarketPriceHandler {
	void handle(String update);
	
	Optional<Price> getLatestPrice(String instrument);

	// this is the JSON endpoint
	String getLatestPriceWeb(String instrument);

}
