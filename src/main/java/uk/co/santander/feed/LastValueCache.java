package uk.co.santander.feed;

import java.util.Optional;

public interface LastValueCache {
	void put(Price price);

	Optional<Price> getInstrument(String instrument);
}
