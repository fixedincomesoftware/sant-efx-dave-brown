package uk.co.santander.feed;

public interface Price {
	String instrument();
	String toJson(String dateTimeFormat);
}
