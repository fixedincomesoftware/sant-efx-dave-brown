package uk.co.santander.feed;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

import javax.inject.Inject;
import javax.inject.Named;

public class CsvParser implements Parser {
	private final String timeStampFormat;
	
	@Inject
	private CsvParser(@Named("dateTimeFormat") String timeStampFormat) {
		this.timeStampFormat = timeStampFormat;
	}
	
	@Override
	public Optional<List<String>> parse(String message) {	
		if (message == null) {
			return Optional.empty();
		}
		
        final StringTokenizer tokeniser = new StringTokenizer(message, ",");
        final int tokenCount = tokeniser.countTokens();
        
        if (tokenCount == 0) {
        	return Optional.empty();
        }
        
        final List<String> values = new ArrayList<>(tokenCount);

        while (tokeniser.hasMoreTokens()){
            values.add(tokeniser.nextToken().trim());
        }

        return Optional.of(values);
	}
	
	@Override
	public Optional<Double> parseDouble(String value) {
		try {
			return Optional.of(Double.parseDouble(value));
		} catch (NumberFormatException nfe) {
			return Optional.empty();
		}
	}
	
	@Override
	public Optional<LocalDateTime> parseDate(String date, String time) {
		final String input = date + " " + time;
		
		try {
			final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeStampFormat);
			final LocalDateTime parsedDateTime = LocalDateTime.parse(input, formatter);
			return Optional.of(parsedDateTime);
		} catch (DateTimeParseException e) {
			return Optional.empty();
		}
	}

}
