package uk.co.santander.feed;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CurrencyPair implements Price {
	private final double bid;
	private final double ask;
	private final String id;
	private final String instrument;
	private final LocalDateTime timeStamp;
	
	public CurrencyPair(String id, String instrument, double bid, double ask, LocalDateTime timeStamp) {
		this.id = id;
		this.instrument = instrument;
		this.bid = bid;
		this.ask = ask;
		this.timeStamp = timeStamp;
	}
	
	@Override
	public int hashCode() {
		return instrument.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		
		if (other instanceof String) {
			return instrument.equals((String)other);
		}
		
		return false;
	}
	
	public String id() {
		return id;
	}
	
	@Override
	public String instrument() {
		return instrument;
	}
	
	public double bid() {
		return bid;
	}
	
	public double ask() {
		return ask;
	}
	
	public LocalDateTime timeStamp() {
		return timeStamp;
	}
	
	@Override
	public String toJson(String dateTimeFormat) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		printOn(pw, dateTimeFormat);
		pw.close();
		return sw.toString();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(instrument);
		sb.append(" bid=" + bid);
		sb.append(" ask=" + ask);
		return sb.toString();
	}

	private void printOn(PrintWriter pw, String dateTimeFormat) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);
		pw.println("");
		pw.println("{");
		pw.println("  id: \"" + id + "\", ");
		pw.println("  instrument: \"" + instrument + "\", ");
		pw.println("  bid: " + bid + ", ");
		pw.println("  ask: " + ask + ", ");
		pw.println("  timestamp: \"" + timeStamp.format(formatter) + "\" ");
		pw.println("}");	
	}
}
