package uk.co.santander.feed;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;

public class MarketPriceHandlerImpl implements MarketPriceHandler {
	private final LastValueCache cache;
	private final Parser parser;
	private final String dateTimeFormat;
	private final double commission;
	
	@Inject
	private MarketPriceHandlerImpl(Parser parser, 
			                       LastValueCache cache, 
			                       @Named("spreadPercent") double spreadPercent,
			                       @Named("dateTimeFormat") String dateTimeFormat) {
		this.parser = parser;
		this.cache = cache;
		this.commission = spreadPercent / 100;
		this.dateTimeFormat = dateTimeFormat;
	}
	
	@Override
	public void handle(String line) {
		Optional<List<String>> update = parser.parse(line);
		
		if (!update.isPresent()) {
			return;
		}
		
		final List<String> values = update.get();
		
		final String id = values.get(0);
		final String instrument = values.get(1);
		final Optional<Double> bid = parser.parseDouble(values.get(2));
		final Optional<Double> ask = parser.parseDouble(values.get(3));
		
		if (!bid.isPresent()) {
			return;
		}
		
		if (!ask.isPresent()) {
			return;
		}
		
		String date = values.get(4);
		String time = values.get(5);
		Optional<LocalDateTime> timeStamp = parser.parseDate(date, time);
		
		if (!timeStamp.isPresent()) {
			return;			
		}
		
		handle(new CurrencyPair(id, instrument, bid.get(), ask.get(), timeStamp.get()));
	}
	
	private void handle(CurrencyPair currencyPair) {
		addCommission(currencyPair);
	}

	// GBP/USD bid=1.249997 ask=1.256003
	// spread before 0.006
	// spread after  0.006006
	private void addCommission(CurrencyPair cp) {
		final double mid = (cp.ask() + cp.bid()) / 2.0d;
		final double halfSpread =  (cp.ask() - mid);
		
		// widen the spread to add the commission
		final double spread = halfSpread * (1 + this.commission);
		
		// adjust the bid and ask price 
		final double newBid = mid - spread;
		final double newAsk = mid + spread;
	
		cache.put(new CurrencyPair(cp.id(), cp.instrument(), newBid, newAsk, cp.timeStamp()));
	}
	
	@Override
	public Optional<Price> getLatestPrice(String instrument) {
		return cache.getInstrument(instrument);
	}

	/**
	 * this is the JSON end point
	 */
	@Override
	public String getLatestPriceWeb(String instrument) {
		final Optional<Price> cp = cache.getInstrument(instrument);
		
		if (!cp.isPresent()) {
			return "{ error: \"instrument not found\" } ";
		}
		
		return cp.get().toJson(dateTimeFormat);
	}
}
