package uk.co.santander.feed;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubscriberImpl implements Subscriber {
	private static final Logger log = LoggerFactory.getLogger(SubscriberImpl.class);
	private final MarketPriceHandler handler;
	
	@Inject
	private SubscriberImpl(MarketPriceHandler handler) {
		this.handler = handler;
	}
	
	@Override
	public void onMessage(String message) {
		log.info("onMessage(" + message + ")");
		
		try (final BufferedReader reader = new BufferedReader(new StringReader(message))) {
			String line = null;
			
			do  {
				line = reader.readLine();
				handler.handle(line);
			} while (line != null);
			
		} catch (IOException ioe) {
			log.error("failed to parse CSV", ioe);
		}
	}
}
