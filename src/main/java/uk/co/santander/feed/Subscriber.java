package uk.co.santander.feed;

public interface Subscriber {
	void onMessage(String message);
}
