package uk.co.santander.feed;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.santander.main.Launcher;

public class LastValueCacheImpl implements LastValueCache {
	private static final Logger log = LoggerFactory.getLogger(Launcher.class);
	
	private Map<String, Price> cache = new ConcurrentHashMap<>();
	
	@Override
	public void put(Price price) {
		log.info("put() " + price);
		cache.put(price.instrument(), price);	
	}

	@Override
	public Optional<Price> getInstrument(String instrument) {
		return Optional.ofNullable(cache.getOrDefault(instrument, null));
	}
}
