package uk.co.santander.main;

import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;

import uk.co.santander.feed.LastValueCache;
import uk.co.santander.feed.Price;
import uk.co.santander.feed.Subscriber;

public class Launcher {
	private static final String EOL = System.getProperty("line.separator");
	
	private final Subscriber subscriber;
	private final LastValueCache cache;
	private static final Logger log = LoggerFactory.getLogger(Launcher.class);
	
	@Inject
	private Launcher(Subscriber subscriber, LastValueCache cache) {
		this.subscriber = subscriber;
		this.cache = cache;
	}
	
	public void launch() {
		log.info("launching ...");
		
		subscriber.onMessage("106, GBP/USD, 1.2500, 1.2560, 01-06-2020, 12:01:01.002");
		
		subscriber.onMessage("107, EUR/JPY, 119.60, 119.90, 01-06-2020, 12:01:02.002" + EOL +
				             "108, GBP/USD, 1.2500, 1.2560, 01-06-2020, 12:01:02.002");
		
		subscriber.onMessage("109, GBP/USD, 1.2499, 1.2561, 01-06-2020, 12:01:02.100" + EOL +
	                         "110, EUR/JPY, 119.61, 119.91, 01-06-2020, 12:01:02.110");
		
		Optional<Price> priceOpt = cache.getInstrument("GBP/USD");
		
		if (priceOpt.isPresent()) {
			log.info("GBP/USD: " + priceOpt.get().toString());
		}
		
	}
	
	public static void main(String... arguments) {
		final Injector injector = Guice.createInjector(new HandlerModule());
		final Launcher launcher = injector.getInstance(Launcher.class);
		launcher.launch();
	}
}
