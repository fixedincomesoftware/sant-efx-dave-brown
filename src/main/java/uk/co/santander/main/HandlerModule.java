package uk.co.santander.main;

import javax.inject.Singleton;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

import uk.co.santander.feed.CsvParser;
import uk.co.santander.feed.LastValueCache;
import uk.co.santander.feed.LastValueCacheImpl;
import uk.co.santander.feed.MarketPriceHandler;
import uk.co.santander.feed.MarketPriceHandlerImpl;
import uk.co.santander.feed.Parser;
import uk.co.santander.feed.Subscriber;
import uk.co.santander.feed.SubscriberImpl;

public class HandlerModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(Subscriber.class).to(SubscriberImpl.class).in(Singleton.class);
		bind(LastValueCache.class).to(LastValueCacheImpl.class).in(Singleton.class);
		bind(Parser.class).to(CsvParser.class).in(Singleton.class);
		bind(MarketPriceHandler.class).to(MarketPriceHandlerImpl.class);
		
		// configure the spread to be 0.1 percent
		bindConstant().annotatedWith(Names.named("spreadPercent")).to(0.1d);
		bindConstant().annotatedWith(Names.named("dateTimeFormat")).to("dd-MM-yyyy HH:mm:ss.SSS");
	}

}
