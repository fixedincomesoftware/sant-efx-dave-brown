package uk.co.santander;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;

import uk.co.santander.feed.Parser;
import uk.co.santander.main.HandlerModule;

public class ParserTest {
	private Parser parser;
	
	@Before
	public void before() {
		final Injector injector = Guice.createInjector(new HandlerModule());
		parser = injector.getInstance(Parser.class);
	}
	
	@After
	public void after() {
		parser = null;
	}
	
	@Test
	public void emptyString() {
		Optional<List<String>> result = parser.parse("");
		assertFalse(result.isPresent());
	}
	
	@Test
	public void nullString() {
		Optional<List<String>> result = parser.parse(null);
		assertFalse(result.isPresent());
	}
	
	@Test
	public void parseLine() {
		Optional<List<String>> result = parser.parse("106, GBP/USD, 1.2500, 1.2560, 01-06-2020, 12:01:02.002");
		assertTrue(result.isPresent());
		final List<String> values = result.get();
		assertEquals(6, values.size());
		assertEquals("106", values.get(0));
		assertEquals("1.2500", values.get(2));
	}
	
	@Test
	public void csvToJavaDate() {
		final Optional<LocalDateTime> result = parser.parseDate("01-06-2020", "12:01:02.002");
		assertTrue(result.isPresent());
		assertEquals(2020, result.get().getYear());
		assertEquals(6, result.get().getMonthValue());
		assertEquals(1, result.get().getDayOfMonth());
		
	}
	
	@Test
	public void goodDouble() {
		final Optional<Double> result = parser.parseDouble("1.2500");
		assertTrue(result.isPresent());
		assertEquals(1.2500d, result.get(), 0.00001d);
	}
	
	@Test
	public void badDouble() {
		final Optional<Double> result = parser.parseDouble("p1.2500");
		assertFalse(result.isPresent());
	}
}
