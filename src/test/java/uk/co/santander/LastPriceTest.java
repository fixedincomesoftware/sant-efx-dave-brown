package uk.co.santander;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;

import uk.co.santander.feed.MarketPriceHandler;
import uk.co.santander.feed.Price;
import uk.co.santander.feed.Subscriber;
import uk.co.santander.main.HandlerModule;

public class LastPriceTest {
	private static final String EOL = System.getProperty("line.separator");
	private Injector injector;
	
	@Before
	public void before() {
		injector = Guice.createInjector(new HandlerModule());
	}
	
	@After
	public void after() {
		injector = null;
	}
	
	@Test
	public void latestPriceFromClient() {
		final MarketPriceHandler handler = injector.getInstance(MarketPriceHandler.class);
		final Subscriber subscriber = injector.getInstance(Subscriber.class);
		
		// message with one line
		subscriber.onMessage("106, GBP/USD, 1.2500, 1.2560, 01-06-2020, 12:01:01.002");
		
		//message with multiple lines
		subscriber.onMessage("107, EUR/JPY, 119.60, 119.90, 01-06-2020, 12:01:02.002" + EOL +
				             "108, GBP/USD, 1.2500, 1.2560, 01-06-2020, 12:01:02.002" + EOL +
				             "109, GBP/USD, 1.2499, 1.2561, 01-06-2020, 12:01:02.100");
		
		final Optional<Price> priceOpt = handler.getLatestPrice("GBP/USD");
		
		assertTrue(priceOpt.isPresent());
		
		// fake web request
		System.out.println(handler.getLatestPriceWeb("GBP/USD"));
	}
}


    
